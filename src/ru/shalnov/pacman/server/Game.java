package ru.shalnov.pacman.server;

import ru.shalnov.pacman.client.gui.MapUI;
import ru.shalnov.pacman.protocol.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill Shalnov
 * Date: 02.04.13
 * Time: 11:05
 */
public class Game implements Runnable{

    LinkedList<Player> mPlayers = new LinkedList<Player>();
    Map mMap;
    MapUI mMapUI;

    final int LoopDelay = 300;

    boolean mIsRunning;

    public Game(Map map, MapUI mapUI) {
        mMap = map;
        mMapUI = mapUI;
    }

    public synchronized Map getMap() {
        return mMap;
    }

    public synchronized int registerPlayer(Player player) throws RegisterError {
        int x = 0, y = 0;
        ArrayList<Integer> point = getEmptyPoint(5);
        x = point.get(0);
        y = point.get(1);
        if (x < 0 || y < 0) {
            throw new RegisterError();
        }
        player.join(this, x, y);
        mPlayers.add(player);
        return player.getUID();
    }

    /**
     * Find point without other players in neighbourhood
     *
     * @param n - number of trying
     * @return list with 2 Integer numbers - x and y
     */
    private ArrayList<Integer> getEmptyPoint(int n) {
        int x = -1;
        int y = -1;
        attempts: for (int i = 0; i < n; i++) {
            int r = 5; //radius
            int side = Math.abs(new Random().nextInt() % 4); //4 sides: top, bottom, left, right
            switch (side) {
                case 0:
                    x = 0;
                    y = Math.abs(new Random().nextInt() % mMap.getHeight());
                    break;
                case 1:
                    x = mMap.getWidth() - 1; //indexing from 0
                    y = Math.abs(new Random().nextInt() % mMap.getHeight());
                    break;
                case 2:
                    x = Math.abs(new Random().nextInt() % mMap.getWidth());
                    y = 0;
                    break;
                case 3:
                    x = Math.abs(new Random().nextInt() % mMap.getWidth());
                    y = mMap.getHeight() - 1; //indexing from 0
                    break;
            }
            for (Player player : mPlayers) {
                 if ( Math.abs(player.getX() - x) < r
                   || Math.abs(player.getY() - y) < r) {
                     x = -1;
                     y = -1;
                     continue attempts;
                 }
            }
            break;
        }

        ArrayList<Integer> point = new ArrayList<Integer>();
        point.add(x);
        point.add(y);
        return point;
    }

    @Override
    public void run() {
        mIsRunning = true;

        try {
             while (mIsRunning)
             {

                Thread.sleep(LoopDelay);

                UpdatePlayers();

                UpdateGameStateInPlayer();
             }

        }
        catch (InterruptedException e) {
            System.out.println("game loop crash");
            e.printStackTrace();
        }
    }

    private void UpdatePlayers()
    {
        for (Player player : mPlayers) {
           player.setMoving(player.mLastStatus.course);



        }
    }

    private void UpdateGameStateInPlayer()
    {
        //create game status
        GameStatus gameStatus = new GameStatus();

        gameStatus.map = getMap();

        for (Player player : mPlayers) {
            if (!player.alive) continue;

            PlayerStatus playerStatus = new PlayerStatus();
            playerStatus.x = player.getX();
            playerStatus.y = player.getY();
            playerStatus.uid = player.getUID();
            playerStatus.type = player.getType();
            gameStatus.playerStatusList.add(playerStatus);
        }

        for (Player player : mPlayers) {
            player.sendGameStatus(gameStatus);
        }

        mMapUI.refresh(gameStatus);
    }


    public class BadCourseException extends Exception {
        //ToDo: something else (=
    }

    public class RegisterError extends Exception {
        @Override
        public String getMessage() {
            return "Can't find empty point";
        }
    }
}