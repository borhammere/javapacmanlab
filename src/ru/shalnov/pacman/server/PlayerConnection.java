package ru.shalnov.pacman.server;

import ru.shalnov.pacman.protocol.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 18:04
 */
public class PlayerConnection implements Runnable {

    //connections
    private Socket mSocket;
    private ObjectOutputStream mOutputStream;
    private ObjectInputStream mInputStream;

    private Player mPlayer;

    private boolean mIsRunning;

    public PlayerConnection(Socket socket, ObjectOutputStream outputStream, ObjectInputStream inputStream,
                            Player player) {
        mPlayer = player;
        mSocket = socket;
        mOutputStream = outputStream;
        mInputStream = inputStream;
    }



    public void sendGameStatus(GameStatus gameStatus) throws IOException {

            System.out.println("Sending game status to " + mPlayer.getUID());

            Message message = new Message();
            message.setMessageType(Protocol.GAMESTATUS);
            message.setBody(TransferHelper.Serializer(gameStatus));

            mOutputStream.writeObject(message);
            mOutputStream.flush();

            System.out.println("success");

    }

    public void sendMap(Map map) {
        System.out.println("Sending the map...");

        Message messMap = new Message();
        try {

            messMap.setMessageType(Protocol.MAP);
            messMap.setBody(TransferHelper.Serializer(map));

            System.out.println("success");
        } catch (Exception e) {
            messMap.setMessageType(Protocol.ERROR);
            messMap.setBody(e.getMessage());
            System.out.println("fail");
            e.printStackTrace();
        } finally {
            try {
                mOutputStream.writeObject(messMap);
                mOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendId() {
        System.out.println("Sending id...");
        try {
        Message messId = new Message();
        messId.setMessageType(Protocol.ID);
        messId.setBody(String.valueOf(mPlayer.getUID()));


            mOutputStream.writeObject(messId);

        mOutputStream.flush();

        System.out.println("success");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void messageProcess(Message receivedMessage)
    {
        try {

            if (receivedMessage.getMessageType().equals(Protocol.EXIT))
            {
                mPlayer.kill();
                mIsRunning = false;
            }

            if (receivedMessage.getMessageType().equals(Protocol.PLAYERSTATUS))
            {
                mPlayer.mLastStatus =
                                (PlayerStatus) TransferHelper.Deserializer(receivedMessage.getBody());
            }

        } catch (Exception e) {
            System.out.println("fail message");
            e.printStackTrace();
        }

    }

    public void stop()
    {
        mIsRunning = false;
    }

    @Override
    public void run() {
        mIsRunning = true;

        try
        {
            //get messages loop
            while (mIsRunning) {
                System.out.println("wait message for " + mPlayer.getUID());
                Message receivedMessage = (Message)mInputStream.readObject();
                System.out.println("message received for " + mPlayer.getUID());
                messageProcess(receivedMessage);

            };

            mSocket.close();
        } catch (Exception e) {

            System.out.println("client loop crash for " + mPlayer.getUID());
            mPlayer.mGame.mPlayers.remove(mPlayer);
            e.printStackTrace();
        }
    }
}
