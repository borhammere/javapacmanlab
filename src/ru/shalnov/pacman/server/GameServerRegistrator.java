package ru.shalnov.pacman.server;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import ru.shalnov.pacman.protocol.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill <borhammere@gmail.com>
 * Date: 04.05.13
 * Time: 22:13
 */
public class GameServerRegistrator implements Runnable {

    Game mGame;
    Map mMap;

    public GameServerRegistrator(Game game) {
        mGame = game;
        mMap = game.getMap();
    }





    private static void sendError(ObjectOutputStream oos, String error) {
        Message messErr = new Message();
        messErr.setMessageType(Protocol.ERROR);
        messErr.setBody(error);
        try {
            oos.writeObject(messErr);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("Registration server started");
        BufferedReader in = null;

        ServerSocket server = null;
        Socket fromclient = null;

        try {
            server = new ServerSocket(4444);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        while (true) {
            try {
                System.out.println("Waiting for a client...");
                fromclient = server.accept();
                System.out.println("Client connected");

                ObjectOutputStream oos = new ObjectOutputStream(fromclient.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(fromclient.getInputStream());
                try {
                    Message receivedMess = (Message) ois.readObject();
                    String name = "anonymous";
                    String type = Protocol.GHOST;
                    if (receivedMess.getMessageType().equals(Protocol.NAME)) {
                        name = receivedMess.getBody();
                    }

                    receivedMess = (Message) ois.readObject();
                    if (receivedMess.getMessageType().equals(Protocol.TYPE)) {
                        type = receivedMess.getBody();
                    }

                    System.out.println("Creating new player...");
                    Player player = new Player(fromclient, oos, ois, type, name);
                    System.out.println("succes");

                    System.out.println("Register new player...");
                    int id = -1;
                    try {
                        id = mGame.registerPlayer(player);
                        System.out.println("succes");

                    } catch (Game.RegisterError e) {
                        System.out.println("fail");
                        sendError(oos, e.getMessage());
                        e.printStackTrace();
                    }

                } catch (ClassNotFoundException e) {
                    System.out.println("fail");
                    e.printStackTrace();
                }

                //oos.close();   do not close, because it will close socket
                //server.close(); never close, thread would have been crushed by the parent
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
