package ru.shalnov.pacman.server;

import ru.shalnov.pacman.client.gui.MapUI;
import ru.shalnov.pacman.protocol.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill Shalnov
 * Date: 27.03.13
 * Time: 1:20
 */
public class Main {

    public static void main(String[] args) {
        /////////////
        //Build map//
        /////////////

        //width, height, density, min step
        System.out.println("Generate map...");
        MapGenerator mapGen = new MapGenerator(20, 10, 0.44, 3);
        Map map = mapGen.generateMap();

        MapUI mapUI = new MapUI();
        mapUI.create(map, -1);

        System.out.println("success");
        Game game = new Game(map, mapUI);
        map.print();

        GameServerRegistrator registrator = new GameServerRegistrator(game);
        Thread registratorThread = new Thread(registrator);
        registratorThread.start();

        Thread gameThread = new Thread(game);
        gameThread.start();

        try {
            registratorThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
