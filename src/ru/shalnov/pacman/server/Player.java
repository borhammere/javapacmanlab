package ru.shalnov.pacman.server;

import ru.shalnov.pacman.client.GameConnection;
import ru.shalnov.pacman.protocol.Courses;
import ru.shalnov.pacman.protocol.GameStatus;
import ru.shalnov.pacman.protocol.PlayerStatus;
import ru.shalnov.pacman.protocol.Protocol;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill Shalnov
 * Date: 02.04.13
 * Time: 10:31
 */
public class Player {
    //count players in the game, to make unique Id
    private static int mCounter = 0;
    //type of person
    private String mType;
    private int UID;
    //game with map. where we playing
    public Game mGame;
    //coordinates
    private int mX;
    private int mY;
    //more - better
    private int mPoints = 0;

    public boolean alive = true;

    public PlayerStatus mLastStatus;

    private String mName = "NoName";

    //Connection to client (or server)
    private PlayerConnection mConnection;
    Thread connectionThread;


    public Player(Socket socket, ObjectOutputStream outputStream, ObjectInputStream inputStream,
                  String type, String name) {
        mType = type;
        mConnection = new PlayerConnection(socket, outputStream, inputStream, this);
        mName = name;

        mLastStatus = new PlayerStatus();

        UID = mCounter++;

    }

    public int getUID() {
        return UID;
    }

    public void setMoving(int course) {
        int tryX = mX;
        int tryY = mY;

        switch (course)
        {
            case Courses.LEFT: tryX-=1;  break;
            case Courses.RIGHT: tryX+=1;  break;
            case Courses.UP: tryY-=1;  break;
            case Courses.DOWN: tryY+=1;  break;
        }

        if (!mGame.mMap.isPassable(tryX, tryY)) return;

        for (Player player : mGame.mPlayers) {
            if (player.UID != UID && player.getX() == tryX && player.getY() == tryY && player.alive)
            {
                if (player.getType().equals(Protocol.GHOST) && mType.equals(Protocol.PACMAN))
                {
                    kill();
                }

                if (player.getType().equals(Protocol.PACMAN) && mType.equals(Protocol.GHOST))
                {
                    player.kill();
                }

                return;
            }
        }

        mX = tryX;
        mY = tryY;

        if (mGame.mMap.hasFood(mX,mY))
        {
            mPoints++;
            mGame.mMap.setFood(mX,mY,false);
        }
    }

    public void join(Game game, int x, int y) {
        mGame = game;
        mX = x;
        mY = y;

        mConnection.sendId();
        mConnection.sendMap(game.getMap());

        Thread connectionThread = new Thread(mConnection);
        connectionThread.start();
    }

    public void kill() {
        alive = false;
    }

    public void sendGameStatus(GameStatus gameStatus)
    {
        try {
            mConnection.sendGameStatus(gameStatus);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

}
