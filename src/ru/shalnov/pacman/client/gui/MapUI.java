package ru.shalnov.pacman.client.gui;

import ru.shalnov.pacman.protocol.Courses;
import ru.shalnov.pacman.protocol.GameStatus;
import ru.shalnov.pacman.protocol.Map;
import ru.shalnov.pacman.protocol.PlayerStatus;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill <borhammere@gmail.com>
 * Date: 26.05.13
 * Time: 19:12
 */
public class MapUI {
    private Map mMap;
    private GameStatus mGameStatus;
    private JFrame mFrame;
    private MapPanel mMapPanel;
    private int mWidth;
    private int mHeight;
    private int mUID;

    private int course = Courses.STAY;

    public int getCurrentCourse() {
        return course;
    }

    public void create(Map map, int UID) {
        mMap = map;
        mUID = UID;

        mWidth = (mMap.getWidth() + 1) * MapPanel.CELL_SIZE;
        mHeight = (mMap.getHeight() + 1) * MapPanel.CELL_SIZE;

        mFrame = new JFrame("Pacman Pro SUPER VMK EDITION");
        if (UID < 0) mFrame.setTitle("SERVER");

        mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mMapPanel = new MapPanel(mMap, UID);
        mFrame.add(mMapPanel);
        mFrame.setSize(mWidth, mHeight);
        mFrame.setMinimumSize(mFrame.getSize());
        mFrame.setMaximumSize(mFrame.getSize());

        mFrame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        course = Courses.UP;
                        break;
                    case KeyEvent.VK_DOWN:
                        course = Courses.DOWN;
                        break;
                    case KeyEvent.VK_LEFT:
                        course = Courses.LEFT;
                        break;
                    case KeyEvent.VK_RIGHT:
                        course = Courses.RIGHT;
                        break;
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        course = Courses.UP;
                        break;
                    case KeyEvent.VK_DOWN:
                        course = Courses.DOWN;
                        break;
                    case KeyEvent.VK_LEFT:
                        course = Courses.LEFT;
                        break;
                    case KeyEvent.VK_RIGHT:
                        course = Courses.RIGHT;
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        course = Courses.UP;
                        break;
                    case KeyEvent.VK_DOWN:
                        course = Courses.DOWN;
                        break;
                    case KeyEvent.VK_LEFT:
                        course = Courses.LEFT;
                        break;
                    case KeyEvent.VK_RIGHT:
                        course = Courses.RIGHT;
                        break;
                }
            }
        });

        mFrame.setVisible(true);
    }

    public void refresh(GameStatus gameStatus) {
        boolean isGameOver = true;
        for (PlayerStatus ps : gameStatus.playerStatusList) {
            if (ps.uid == mUID) {
                isGameOver = false;
                break;
            }
        }
        if (isGameOver && mUID >= 0) {
            mFrame.setTitle("GAME OVER");
        }
        mMapPanel.refresh(gameStatus);
    }

}
