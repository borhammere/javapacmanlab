package ru.shalnov.pacman.client.gui;

import ru.shalnov.pacman.protocol.GameStatus;
import ru.shalnov.pacman.protocol.Map;
import ru.shalnov.pacman.protocol.PlayerStatus;
import ru.shalnov.pacman.protocol.Protocol;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill <borhammere@gmail.com>
 * Date: 26.05.13
 * Time: 19:43
 */
public class MapPanel extends JPanel {

    public static final int CELL_SIZE = 20;

    private Image mBackgroundImg = new ImageIcon("res/background.jpg").getImage();
    private Image mGhostImg = new ImageIcon("res/ghost.png").getImage();
    private Image mPacmanImg = new ImageIcon("res/pacman.png").getImage();
    private Image mPlayerImg = new ImageIcon("res/player.png").getImage();

    private Map mMap;
    private int mUID;
    private GameStatus mGameStatus = new GameStatus();

    public MapPanel(Map map, int UID) {
        super();
        mUID = UID;
        mMap = map;
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(mBackgroundImg, 0, 0, (mMap.getWidth() + 1) * CELL_SIZE, (mMap.getHeight() + 1) * CELL_SIZE, null);
        for (int i = 0; i < mMap.getHeight(); i++) {
            for (int j = 0; j < mMap.getWidth(); j++) {
                if (mMap.isPassable(j, i)) {
                    g.setColor(Color.darkGray);
                    g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    if (mMap.hasFood(j, i)) {
                        g.setColor(Color.YELLOW);
                        g.fillRect(j * CELL_SIZE + 1 + CELL_SIZE / 8 * 3, i * CELL_SIZE + CELL_SIZE / 8 * 3, CELL_SIZE / 4, CELL_SIZE / 4);
                    }
                }
            }
        }

        for(PlayerStatus ps : mGameStatus.playerStatusList) {
            Image img;
            img = Protocol.GHOST.equals(ps.type) ? mGhostImg : mPacmanImg;
            img = (ps.uid == mUID) ? mPlayerImg : img;
            g.drawImage(img, ps.x * CELL_SIZE, ps.y * CELL_SIZE, CELL_SIZE, CELL_SIZE, null);
        }
    }

    public void refresh(GameStatus gameStatus) {
        mGameStatus = gameStatus;
        mMap = mGameStatus.map;

        repaint();
    }
}
