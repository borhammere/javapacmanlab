package ru.shalnov.pacman.client;

import java.io.*;

public class MainClient{

    public static void main(String[] args) throws IOException {

        System.out.println("Welcome to Client side");

        Game game = new Game(args[0]);

        Thread connectionThread = new Thread(game);
        connectionThread.start();

        try {
            connectionThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



}
