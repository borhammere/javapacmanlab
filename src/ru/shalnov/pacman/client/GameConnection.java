package ru.shalnov.pacman.client;

import ru.shalnov.pacman.client.Events.ConnectionCompletedEvent;
import ru.shalnov.pacman.protocol.Events.EventSource;
import ru.shalnov.pacman.client.Events.GameStatusReceivedEvent;
import ru.shalnov.pacman.client.Events.ServerErrorEvent;
import ru.shalnov.pacman.protocol.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 13:52
 */
public class GameConnection implements Runnable {
    //connection settings
    private String mHost;
    private int mPort;

    //connections
    private Socket mSocket;
    private ObjectOutputStream mOutputStream;
    private ObjectInputStream mInputStream;

    private String mClientType;


    private boolean mIsRunning;

    //events
    public EventSource ConnectionCompleted;
    public EventSource ServerError;
    public EventSource ChangeGameStatus;

    public GameConnection(String host, int port, String clientType)
    {
        mHost = host;
        mPort = port;
        mClientType = clientType;

        ConnectionCompleted = new EventSource();
        ServerError = new EventSource();
        ChangeGameStatus = new EventSource();
    }

    public void sendNewStatus(PlayerStatus playerStatus)
    {
        try {
            Message messName = new Message();
            messName.setMessageType(Protocol.PLAYERSTATUS);
            messName.setBody(TransferHelper.Serializer(playerStatus));

            mOutputStream.writeObject(messName);
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendName(String name) {
        try {
            Message messName = new Message();
            messName.setMessageType(Protocol.NAME);
            messName.setBody(name);

            mOutputStream.writeObject(messName);
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendType(String type) {
        try {
            Message messType = new Message();
            messType.setMessageType(Protocol.TYPE);
            messType.setBody(type);

            mOutputStream.writeObject(messType);
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop()
    {
        mIsRunning = false;
    }

    private void initialize() throws IOException {
        mSocket = new Socket(mHost, mPort);

        mOutputStream = new ObjectOutputStream(mSocket.getOutputStream());
        mInputStream = new ObjectInputStream(mSocket.getInputStream());
    }

    private void registration() throws IOException, ClassNotFoundException {
        String uid = null;
        Map map = null;

        System.out.println("Sending name...");
        String name = "***CYBERSHOOTER666***";
        sendName(name);
        System.out.println("success");

        System.out.println("Sending type...");
        sendType(mClientType);
        System.out.println("success");

        System.out.println("Recieving UID...");
        Message recievedMess = (Message) mInputStream.readObject();
        if (recievedMess.getMessageType().equals(Protocol.ID)) {

            uid = recievedMess.getBody();

            System.out.println("success");
        } else {
            MessageProcess(recievedMess);
            return;
        }

        System.out.println("Recieving map...");
        recievedMess = (Message) mInputStream.readObject();
        if (recievedMess.getMessageType().equals(Protocol.MAP)) {

            String mapStr = recievedMess.getBody();
            map = TransferHelper.Deserializer(mapStr);

            System.out.println("success");
        } else {
            MessageProcess(recievedMess);
            return;
        }

        ConnectionCompleted.invoke(new ConnectionCompletedEvent(this, uid, map));
    }

    private void MessageProcess(Message receivedMessage)
    {
        try {

            if (receivedMessage.getMessageType().equals(Protocol.ERROR))
            {
                ServerError.invoke(new ServerErrorEvent(this, receivedMessage.getBody()));
            }

            if (receivedMessage.getMessageType().equals(Protocol.GAMESTATUS))
            {
                ChangeGameStatus.invoke(
                        new GameStatusReceivedEvent(this,
                                (GameStatus)TransferHelper.Deserializer(receivedMessage.getBody())));
            }

        } catch (Exception e) {
            System.out.println("fail message");
            e.printStackTrace();
        }

    }

    @Override
    public void run()
    {
        mIsRunning = true;
        try
        {
            initialize();
            registration();

            //get messages loop
            while (mIsRunning) {
                System.out.println("wait message...");
                Message receivedMessage = (Message)mInputStream.readObject();
                System.out.println("message received, start process");
                MessageProcess(receivedMessage);
            };

            mSocket.close();
        } catch (Exception e) {
            System.out.println("connection loop crash");
            e.printStackTrace();
        }


    }
}
