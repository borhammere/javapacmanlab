package ru.shalnov.pacman.client;

import ru.shalnov.pacman.client.Events.ConnectionCompletedEvent;
import ru.shalnov.pacman.client.gui.MapUI;
import ru.shalnov.pacman.protocol.Events.EventListener;
import ru.shalnov.pacman.client.Events.GameStatusReceivedEvent;
import ru.shalnov.pacman.client.Events.ServerErrorEvent;
import ru.shalnov.pacman.protocol.GameStatus;
import ru.shalnov.pacman.protocol.Map;
import ru.shalnov.pacman.protocol.PlayerStatus;
import ru.shalnov.pacman.protocol.Protocol;
import ru.shalnov.pacman.server.Player;

import java.util.EventObject;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 13:51
 */
public class Game implements Runnable, EventListener {

    //connection settings
    private final  String mHost = "localhost";
    private final int mPort = 4444;
    private GameConnection mConnection;

    private boolean mIsRunning;

    private int mUID;

    private String mClientType;

    private Map mMap;
    private MapUI mapUI;

    Random random;

    private GameStatus mGameStatus;

    public Game(String clientType)
    {
        random = new Random();
        mClientType = clientType;

    }

    private void configureConnection()
    {
        System.out.println("Create connection thread for " + mHost);
        mConnection = new GameConnection(mHost,mPort, mClientType);
        mConnection.ConnectionCompleted.addEventListener(this);
        mConnection.ServerError.addEventListener(this);
        mConnection.ChangeGameStatus.addEventListener(this);

        Thread connectionThread = new Thread(mConnection);
        connectionThread.start();
    }

    @Override
    public void run() {
        mIsRunning = true;

        try {
            configureConnection();

            while (mIsRunning) {
                if (mapUI != null && mGameStatus != null) {
                    mapUI.refresh(mGameStatus);

                    int course = 0;

                    if (mClientType.equals(Protocol.PACMAN))
                        course = mapUI.getCurrentCourse();
                    else course = random.nextInt(4);

                    for (PlayerStatus player : mGameStatus.playerStatusList) {
                        if (player.uid == mUID)
                        {
                            PlayerStatus ps = new PlayerStatus();
                            ps.x = player.x;
                            ps.y = player.y;
                            ps.type = player.type;
                            ps.uid = mUID;
                            ps.course = course;

                            mConnection.sendNewStatus(ps);

                        }
                    }

                }
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            System.out.println("game loop crash");
            e.printStackTrace();
        }

        mConnection.stop();
    }

    @Override
    public void handleEvent(EventObject event) {

        if (event instanceof ConnectionCompletedEvent)
        {
            connectionCompleted((ConnectionCompletedEvent) event);
        }

        if (event instanceof ServerErrorEvent)
        {
            handleServerError((ServerErrorEvent) event);
        }

        if (event instanceof GameStatusReceivedEvent)
        {
            handleChangeGameStatus((GameStatusReceivedEvent) event);
        }
    }

    private void connectionCompleted(ConnectionCompletedEvent event)
    {
        System.out.println("Connection completed: ");
        System.out.println("UID: " + event.UID);
        event.Map.print();

        mUID = Integer.valueOf(event.UID);
        mMap = event.Map;
        mapUI = new MapUI();
        mapUI.create(mMap, mUID);
    }

    private void handleServerError(ServerErrorEvent event)
    {
        System.out.println("Server error: " + event.Message);

    }

    private void handleChangeGameStatus(GameStatusReceivedEvent event)
    {
        System.out.println("Change game status: ");
        System.out.println("Player count: " + event.GameStatus.playerStatusList.size());

        mGameStatus = event.GameStatus;

        for (PlayerStatus player : mGameStatus.playerStatusList) {
            System.out.println("Player coore: " + player.x + "," + player.y);
        }

        mapUI.refresh(mGameStatus);




    }
}
