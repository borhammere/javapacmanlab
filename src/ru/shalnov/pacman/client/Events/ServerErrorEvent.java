package ru.shalnov.pacman.client.Events;

import java.util.EventObject;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 16:01
 */
public class ServerErrorEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException
     *          if source is null.
     */

    public String Message;

    public ServerErrorEvent(Object source, String message) {
        super(source);

        Message = message;
    }
}
