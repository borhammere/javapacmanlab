package ru.shalnov.pacman.client.Events;

import ru.shalnov.pacman.protocol.GameStatus;

import java.util.EventObject;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 16:54
 */
public class GameStatusReceivedEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException
     *          if source is null.
     */

    public GameStatus GameStatus;

    public GameStatusReceivedEvent(Object source, GameStatus gameStatus) {
        super(source);

        GameStatus = gameStatus;
    }
}
