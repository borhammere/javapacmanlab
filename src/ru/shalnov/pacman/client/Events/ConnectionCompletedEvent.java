package ru.shalnov.pacman.client.Events;

import ru.shalnov.pacman.protocol.Map;

import java.util.EventObject;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 14:16
 */
public class ConnectionCompletedEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException
     *          if source is null.
     */

    public String UID;
    public Map Map;

    public ConnectionCompletedEvent(Object source, String uid, Map map) {
        super(source);

        UID = uid;
        Map = map;
    }
}
