package ru.shalnov.pacman.protocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 16:55
 */
public class GameStatus implements Serializable {
    public Map map;
    public ArrayList<PlayerStatus> playerStatusList = new ArrayList<PlayerStatus>();
}
