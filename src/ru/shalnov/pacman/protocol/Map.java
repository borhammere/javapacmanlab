package ru.shalnov.pacman.protocol;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill
 * Date: 27.03.13
 * Time: 1:34
 */
public class Map implements Serializable {

    private static final long serialVersionUID = 1;

    private int mHeight = 10;
    private int mWidth  = 10;
    //passability of map`s points
    private boolean[][] mPassability;
    private boolean[][] mHasFood;

    public Map (int width, int height) {
        mHeight = height;
        mWidth  = width;
        mPassability = new boolean[mWidth][mHeight];
        mHasFood     = new boolean[mWidth][mHeight];
    }

    public void setFood(int x, int y, boolean hasFood) {
        mHasFood[x][y] = hasFood;
    }

    public boolean hasFood(int x, int y) {
        return mHasFood[x][y];
    }

    public void setPassable(int x, int y, boolean passable) {
        mPassability[x][y] = passable;
    }

    public boolean isPassable(int x, int y) {
        if (x<0 || x >= mWidth || y<0 || y>=mHeight) return false;

        return mPassability[x][y];
    }

    public int getHeight() {
        return mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public void print() {
        for (int j = 0; j < this.getHeight(); j++) {
            for (int i = 0; i < this.getWidth(); i++) {
                char sign = this.isPassable(i, j) ? '*' : ' ';
                System.out.print(" " + sign);
            }
            System.out.println();
        }
    }
}
