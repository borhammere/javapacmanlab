package ru.shalnov.pacman.protocol.Events;

import java.util.EventObject;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 16:08
 */
public interface EventListener {
    public void handleEvent(EventObject event);
}
