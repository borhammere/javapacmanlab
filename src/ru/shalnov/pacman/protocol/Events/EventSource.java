package ru.shalnov.pacman.protocol.Events;

import ru.shalnov.pacman.protocol.Events.EventListener;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 16:05
 */
public class EventSource {
    private List _listeners = new ArrayList();
    public synchronized void addEventListener(EventListener listener)  {
        _listeners.add(listener);
    }
    public synchronized void removeEventListener(EventListener listener)   {
        _listeners.remove(listener);
    }


    public synchronized void invoke(EventObject event) {
        Iterator i = _listeners.iterator();
        while(i.hasNext())  {
            ((EventListener) i.next()).handleEvent(event);
        }
    }
}
