package ru.shalnov.pacman.protocol;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill <borhammere@gmail.com>
 * Date: 24.05.13
 * Time: 16:56
 */
public class Message implements Serializable {

    private String mMessageType = Protocol.ERROR;
    private String mBody;

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public String getMessageType() {
        return mMessageType;
    }

    public void setMessageType(String messageType) {
        this.mMessageType = messageType;
    }
}
