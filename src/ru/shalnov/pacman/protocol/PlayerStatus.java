package ru.shalnov.pacman.protocol;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 13:46
 */
public class PlayerStatus implements Serializable {
    //coordinates
    public int x;
    public int y;
    public int course;
    //type of person
    public String type;
    public int uid;
}
