package ru.shalnov.pacman.protocol;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey
 * Date: 26.05.13
 * Time: 17:02
 */
public class TransferHelper {
    public static <T> String Serializer(T obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream tmpOos = new ObjectOutputStream(baos);
        tmpOos.writeObject(obj);
        String objInString = Base64.encode(baos.toByteArray());
        baos.close();
        return objInString;
    }

    public static <T> T Deserializer(String objInString) throws IOException, ClassNotFoundException {
        byte[] decoding = Base64.decode(objInString);
        ByteArrayInputStream bais = new ByteArrayInputStream(decoding);
        ObjectInputStream tmpOis = new ObjectInputStream(bais);

       return  (T) tmpOis.readObject();
    }
}
