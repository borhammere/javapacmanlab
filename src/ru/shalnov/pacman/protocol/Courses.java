package ru.shalnov.pacman.protocol;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill Shalnov
 * Date: 02.04.13
 * Time: 10:40
 */
public abstract class Courses {
    public static final int RIGHT = 0;
    public static final int UP = 1;
    public static final int LEFT = 2;
    public static final int DOWN = 3;
    public static final int STAY = 4;
}
