package ru.shalnov.pacman.protocol;

/**
 * Created with IntelliJ IDEA.
 * User: Shalnov Kirill <borhammere@gmail.com>
 * Date: 08.05.13
 * Time: 22:35
 */
public class Protocol {
    //Message types
    public static String MAP = "map";
    public static String ID = "id";
    public static String GAMESTATUS = "gamestatus";
    public static String PLAYERSTATUS = "playerstatus";
    public static String ERROR = "error";
    public static String NAME = "name";
    public static String TYPE = "type";
    public static String EXIT = "exit";

    //Personage type
    public static String PACMAN = "pacman";
    public static String GHOST = "ghost";
}
